Inventory System

Inventory system based on CRUD interface using items REST API.

## Installation

```python
# the .env is not in repository, laravel has a .env.example file. copy that file.

# Generate key 
php artisan key:generate 

# copy above generated key into .env file where APP_KEY= your key here

# create database with any name and copy that to .env file with host, username and password
# install database tables
php artisan migrate

# start laravel application
php artisan serve

# run testcase
php artisan test
```
## Using docker compose
In order to use, docker compose, you need to have docker installed according to your operating system.

https://docs.docker.com/

After installing docker, go to project source dir e.g

/var/www/laravel.inventory.system and then run sudo docker-compose up -d

After that, run docker ps to get container id and login into that using following command.

sudo docker exec -it <<app-docker-container-id> bash

now, you will be logged into app docker container and run following command,

php artisan serve

Now, you should be able to see app at http://127.0.0.1:8080/

## API Usange

please refer following link for API documentation/usage.

https://documenter.getpostman.com/view/16690462/TzmChYrL 
