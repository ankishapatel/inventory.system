<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use Validator;

class ItemController extends Controller
{
    public function getItemList() {
        $items = Item::get();
        return response([
                        "data" => $items,
                        "count" => $items->count()
                        ], 200);
    }

    public function createItem(Request $request) {
        $validatedData = $request->validate([
                    'name'=> ['required', 'min:2','max:255'],
                    'price'=> ['required', 'min:0', 'numeric'],
                    'quantity'=> ['required', 'integer']
                ],
                [
                    'name.max' => 'Item name should not be greater than 255 characters.',
                ]);

        $item = new Item;
        $item->name = $request->name;
        $item->price = $request->price;
        $item->quantity = $request->quantity;
        $item->save();

        return response()->json([
          "message" => "Item has been created successfully",
          "data" => $item
        ], 201);
    }

    public function getItem($id) {
        if (Item::where('id', $id)->exists()) {
            $item = Item::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
            return response($item, 200);
        } else {
            return response()->json([
                "message" => "Item not found"
            ], 404);
        }
    }

    public function updateItem(Request $request, $id) {
        if (Item::where('id', $id)->exists()) {
            $validatedData = $request->validate([
                        'name'=> ['required', 'min:2','max:255'],
                        'price'=> ['required', 'min:0', 'numeric'],
                        'quantity'=> ['required', 'integer']
                    ],
                    [
                        'name.max' => 'Item name should not be greater than 255 characters.',
                    ]);

            $item = Item::find($id);
            $item->name = is_null($request->name) ? $item->name : $request->name;
            $item->price = is_null($request->price) ? $item->price : $request->price;
            $item->quantity = is_null($request->quantity) ? $item->quantity : $request->quantity;
            $item->save();

            return response()->json([
                "message" => "records updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Item not found"
            ], 404);
        }
    }

    public function deleteItem ($id) {
        if(Item::where('id', $id)->exists()) {
            $item = Item::find($id);
            $item->delete();

            return response()->json([
                "message" => "records deleted"
            ], 202);
        } else {
            return response()->json([
                "message" => "Item not found"
            ], 404);
        }
    }
}
