<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::post('register', 'AuthController@register')->name('register');
Route::post('login', 'AuthController@login')->name('auth.login');
Route::middleware('auth:sanctum')->group(function () {
    Route::get('items', 'ItemController@getItemList')->name('getItemList');
    Route::get('items/{id}', 'ItemController@getItem')->name('getItem');
    Route::post('items', 'ItemController@createItem')->name('createItem');
    Route::put('items/{id}', 'ItemController@updateItem')->name('updateItem');
    Route::delete('items/{id}','ItemController@deleteItem')->name('deleteItem');
});
