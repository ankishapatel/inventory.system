<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class AuthTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     * Test registration
     */
    public function testRegister(){

        $this->withoutExceptionHandling();

        //User's data
        $data = [
            'email' => 'testcase@gmail.com',
            'name' => 'TestCase',
            'password' => 'secret1234',
            'password_confirmation' => 'secret1234',
        ];
        //Send post request
        $response = $this->json('POST',route('register'),$data);
        //Assert it was successful
        $response->assertStatus(200);
        //Assert we received a token
        $this->assertArrayHasKey('token',$response->json());
        //Delete data
        User::where('email','testcase@gmail.com')->delete();
    }

    /**
     * @test
     * @expectException
     * Test registration_fails_empty_parameter
     */
    public function registration_fails_empty_parameter(){

        //Send post request
        $this->json('POST', 'api/register',  ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "name" => ["The name field is required."],
                    "email" => ["The email field is required."],
                    "password" => ["The password field is required."],
                ]
            ]
        );
    }

    /**
     * @test
     * @expectException
     * Test registration_fails_email_password_missing
     */
    public function registration_fails_email_password_missing(){

        //User's data
        $data = [
            'name' => 'TestCase'
        ];
        //Send post request
        $this->json('POST', 'api/register',  ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    'email' => ["The email field is required."],
                    'password' => ["The password field is required."],
                ]
            ]
        );
    }

    /**
     * @test
     * @expectException
     * Test registration_fails_passwrod_missmatch
     */
    public function registration_fails_passwrod_missmatch(){

        //  User's data
        $data = [
                    "name" => "TestCase",
                    "email" => "testcase@gmail.com",
                    "password" => "secret1234"
                ];
        //Send post request
        $this->json('POST', 'api/register', $data, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "password" => ["The password confirmation does not match."]
                ]
            ]
        );
    }
}
