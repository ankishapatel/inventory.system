<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Item;

class ItemTest extends TestCase
{
    use RefreshDatabase;

    //  Get access token
    protected function getAccessToken(){
        $data = [
            'name' => 'TestCase',
            'email' => 'testcase@gmail.com',
            'password' => 'secret1234',
            'password_confirmation' => 'secret1234',
        ];
        //Send post request
        $response = $this->json('POST',route('register'),$data);

        $token = $response->json('token');
        return $token;
    }

    /**
     * @test
     * Test create Item
     */
    public function testCreateItem()
    {
        //Get token
        $token = $this->getAccessToken();

        $data = [
            'name' => 'Notebook',
            'price' => '4.99',
            'quantity' => '100'
        ];

        //call route and assert response
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST',route('createItem'), $data)
            ->assertStatus(201)
            ->assertJson([
                "message" => "Item has been created successfully",
                "data" => $data
                ]);
    }

    /**
     * @test
     * testCreateItem_required_fields
     */
    public function testCreateItem_required_fields()
    {
        //Get token
        $token = $this->getAccessToken();

        $data = [
            /*'name' => 'Notebook',
            'price' => '4.99',
            'quantity' => '100'*/
        ];

        //call route and assert response
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST',route('createItem'), $data)
            ->assertStatus(422)
            ->assertJson([
                    "message" => "The given data was invalid.",
                    "errors" => [
                        "name" => ["The name field is required."],
                        "price" => ["The price field is required."],
                        "quantity" => ["The quantity field is required."]
                    ]
                ]);
    }

    /**
     * @test
     * testCreateItem_price_quantity_should_number
     */
    public function testCreateItem_price_quantity_should_number()
    {
        //Get token
        $token = $this->getAccessToken();

        $data = [
            'name' => 'Notebook',
            'price' => 'abc',
            'quantity' => '1.5'
        ];

        //call route and assert response
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST',route('createItem'), $data)
            ->assertStatus(422)
            ->assertJson([
                    "message" => "The given data was invalid.",
                    "errors" => [
                        "price" => ["The price must be a number."],
                        "quantity" => ["The quantity must be an integer."],
                    ]
                ]);
    }

    /**
     * @test
     * testCreateItem_price_should_be_positive
     */
    public function testCreateItem_price_should_be_positive()
    {
        //Get token
        $token = $this->getAccessToken();

        $data = [
            'name' => 'Notebook',
            'price' => '-5.99',
            'quantity' => '100'
        ];

        //call route and assert response
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('POST',route('createItem'), $data)
            ->assertStatus(422)
            ->assertJson([
                    "message" => "The given data was invalid.",
                    "errors" => [
                        "price" => ["The price must be at least 0."],
                    ]
                ]);
    }

    /**
     * @test
     * Test get all item
     */
    public function testGetItemList(){
        //Get token
        $token = $this->getAccessToken();

        $items = Item::factory()->count(2)->create();

        //call route and assert response
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('GET',route('getItemList'))
            ->assertStatus(200)
            ->assertJson([
                "count" => count($items)
            ]);

    }

    /**
     * @test
     * Test get an item by id
     */
    public function testGetItem(){
        //Get token
        $token = $this->getAccessToken();

        $items = Item::factory()->count(5)->create();

        //call route and assert response
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '. $token,
        ])->json('GET', 'api/items/'.$items->first()->id)
            ->assertStatus(200)
            ->assertJson([]);

    }
}
